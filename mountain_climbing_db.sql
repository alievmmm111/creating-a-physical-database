-- Create the database
CREATE DATABASE mountain_climbing_db;

-- Connect to the database
\c mountain_climbing_db;

-- Create schema
CREATE SCHEMA mountain_climbing_schema;

-- Use the schema
SET search_path TO mountain_climbing_schema;

-- Create tables
CREATE TABLE Gear (
    gear_id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    description VARCHAR(50),
    quantity INT NOT NULL DEFAULT 0 CHECK (quantity >= 0)
);

CREATE TABLE Climber (
    climber_id BIGSERIAL PRIMARY KEY,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL,
    date_of_birth DATE NOT NULL CHECK (date_of_birth > '2000-01-01'),
    email VARCHAR(120) UNIQUE NOT NULL,
    phone VARCHAR(15) UNIQUE
);

CREATE TABLE Expedition (
    expedition_id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    start_date DATE NOT NULL CHECK (start_date > '2000-01-01'),
    end_date DATE NOT NULL CHECK (end_date > '2000-01-01'),
    mountain_id INT REFERENCES Mountain(mountain_id)
);

CREATE TABLE Climber_Gear (
    climber_gear_id SERIAL PRIMARY KEY,
    climber_id BIGINT REFERENCES Climber(climber_id),
    gear_id INT REFERENCES Gear(gear_id),
    quantity INT NOT NULL DEFAULT 1 CHECK (quantity > 0)
);

CREATE TABLE Climber_Expedition (
    climber_expedition_id SERIAL PRIMARY KEY,
    climber_id BIGINT REFERENCES Climber(climber_id),
    expedition_id INT REFERENCES Expedition(expedition_id),
    role VARCHAR(64)
);

CREATE TABLE Mountain (
    mountain_id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    region_id INT REFERENCES Region(region_id),
    elevation VARCHAR(80) NOT NULL
);

CREATE TABLE Region (
    region_id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL
);

-- Add record_ts field using ALTER TABLE
ALTER TABLE Gear ADD COLUMN record_ts TIMESTAMP DEFAULT current_timestamp;
ALTER TABLE Climber ADD COLUMN record_ts TIMESTAMP DEFAULT current_timestamp;
ALTER TABLE Expedition ADD COLUMN record_ts TIMESTAMP DEFAULT current_timestamp;
ALTER TABLE Climber_Gear ADD COLUMN record_ts TIMESTAMP DEFAULT current_timestamp;
ALTER TABLE Climber_Expedition ADD COLUMN record_ts TIMESTAMP DEFAULT current_timestamp;
ALTER TABLE Mountain ADD COLUMN record_ts TIMESTAMP DEFAULT current_timestamp;
ALTER TABLE Region ADD COLUMN record_ts TIMESTAMP DEFAULT current_timestamp;

-- Inserting into Gear table
INSERT INTO Gear (name, description, quantity) VALUES ('Climbing Rope', 'Dynamic and strong', 15);
INSERT INTO Gear (name, description, quantity) VALUES ('Harness', 'Adjustable and secure', 20);

-- Inserting into Climber table
INSERT INTO Climber (first_name, last_name, date_of_birth, email, phone) VALUES ('Jane', 'Smith', '1992-05-15', 'jane.smith@example.com', '123-456-7890');
INSERT INTO Climber (first_name, last_name, date_of_birth, email, phone) VALUES ('Michael', 'Johnson', '1985-11-20', 'michael.johnson@example.com', '987-654-3210');

-- Inserting into Expedition table
INSERT INTO Expedition (name, start_date, end_date, mountain_id) VALUES ('Everest Expedition', '2023-04-01', '2023-05-31', 1);
INSERT INTO Expedition (name, start_date, end_date, mountain_id) VALUES ('K2 Summit', '2023-06-15', '2023-08-10', 2);

-- Inserting into Climber_Gear table
INSERT INTO Climber_Gear (climber_id, gear_id, quantity) VALUES (1, 1, 2);
INSERT INTO Climber_Gear (climber_id, gear_id, quantity) VALUES (2, 2, 1);

-- Inserting into Climber_Expedition table
INSERT INTO Climber_Expedition (climber_id, expedition_id, role) VALUES (1, 1, 'Lead Climber');
INSERT INTO Climber_Expedition (climber_id, expedition_id, role) VALUES (2, 2, 'Support Team');

-- Inserting into Mountain table
INSERT INTO Mountain (name, region_id, elevation) VALUES ('Mount Everest', 1, '29,032 ft (8,849 m)');
INSERT INTO Mountain (name, region_id, elevation) VALUES ('K2', 2, '28,251 ft (8,611 m)');

-- Inserting into Region table
INSERT INTO Region (name) VALUES ('Himalayas');
INSERT INTO Region (name) VALUES ('Karakoram');
